/// set_pos_to_midpoint(instance_type);

var instanceType = argument0;

var instanceCount = instance_number(instanceType)

if (instanceCount > 0)
{   
    midx = 0;
    midy = 0;


    with (instanceType)
    {
        other.midx += x;
        other.midy += y;
    }
    
    x = midx / instanceCount;
    y = midy / instanceCount;
}
