#define load_next_level
/// load_next_level(): Boolean

var nextLevel = "l" + string(++global.levelId) +  ".json";

if (file_exists(nextLevel))
{
    var level = file_text_open_read(nextLevel);
    var levelStr = "";
    while (!file_text_eof(level))
    {
        levelStr += file_text_readln(level);
    }
    
    file_text_close(level);
    
    global.levelData = levelStr;

    return true;
}
else
{
    return false;
}

#define prep_level
var decoded = json_decode(global.levelData);

var layers = ds_map_find_value(decoded, "layers");
var layer = ds_list_find_value(layers, 0);
var height = ds_map_find_value(layer, "height");
var width = ds_map_find_value(layer, "width");

room_set_width(game_room, width * 32 + 20 * 32) 
room_set_height(game_room, height * 32 + 20 * 32)

room_set_width(game_room, width * 32 + 20 * 32) 
room_set_height(game_room, height * 32 + 20 * 32)