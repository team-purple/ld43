#define load_level
/// load_level(str)

var str = argument0;

var levelData = json_decode(str);

var layers = ds_map_find_value(levelData, "layers");
var layer = ds_list_find_value(layers, 0);
var height = ds_map_find_value(layer, "height");
var width = ds_map_find_value(layer, "width");

var tileData = ds_map_find_value(layer, "data");

room_set_width(game_room, width * 32 + 20 * 32) 
room_set_height(game_room, height * 32 + 20 * 32)

var i, j;
for (i = 0; i < width; i++)
{
    for (j = 0; j < height; j++)
    {
        var tileId = ds_list_find_value(tileData, i + j * width);
        load_tile(10 * 32 + i * 32, 10 * 32 + j * 32, real(tileId) - 1);
    }
}

#define load_tile
/// load_tile(x, y, id)

var xx = argument0;
var yy = argument1;
var tileId = argument2;

switch(tileId)
{
    case 0:
        var instance = instance_create(xx, yy, wall32);
        break;
    case 1:
        var instance = instance_create(xx, yy, tree);
        break;
    case 3:
        // Blue door
        var instance = instance_create(xx, yy, door);
        with (instance)
        {
            connectId = 2;
            sprite_index = ssdoorBlue;
        }
        break;
    case 5:
        // Red door
        var instance = instance_create(xx, yy, door);
        with (instance)
        {    
            connectId = 1;
            sprite_index = ssdoorRed;
        }
        break;
    case 7:
        //var instance = instance_create(xx, yy, water);
        break;
    case 9:
        var instance = instance_create(xx, yy, lemmingStart);
        break;
    case 68:
        var instance = instance_create(xx, yy, goal);
        break;
    case 14:
        var instance = instance_create(xx, yy, rock);
        break;
    case 48:
        var instance = instance_create(xx, yy, spikePit);
        break;
    case 50:
        // Blue button
        var instance = instance_create(xx, yy, button);
        with (instance)
        {
            connectId = 2;
            sprite_index = ssbuttonBlue;
        }
        break;
    case 52:
        // Red button
        var instance = instance_create(xx, yy, button);
        with (instance)
        {
            connectId = 1;
            sprite_index = ssbuttonRed;
        }
        break;
    case 54:
        var instance = instance_create(xx, yy, slowzone);
        break;
    case 64:
        var instance = instance_create(xx, yy, timedSpikes);
        
        with (instance)
        {
            alarm[0] = 1;
        }
        break;
    case 66:
        var instance = instance_create(xx, yy, timedSpikes);
        
        with (instance)
        {
            alarm[1] = 1;
        }
        break;
    // platform 1
    case 16:
        create_platform_node(1, 1, xx, yy);
        var instance = instance_create(xx, yy, movingPlatform);
        with (instance)
        {
            platformId = 1;
        }
        break;
    case 17:
        create_platform_node(1, 2, xx, yy);
        break;
    case 18:
        create_platform_node(1, 3, xx, yy);
        break;
    case 19:
        create_platform_node(1, 4, xx, yy);
        break;
    case 20:
        create_platform_node(1, 5, xx, yy);
        break;
    case 21:
        create_platform_node(1, 6, xx, yy);
        break;

    // platform 2
    case 24:
        create_platform_node(2, 1, xx, yy);
        var instance = instance_create(xx, yy, movingPlatform);
        with (instance)
        {
            platformId = 2;
        }
        break;
    case 25:
        create_platform_node(2, 2, xx, yy);
        break;
    case 26:
        create_platform_node(2, 3, xx, yy);
        break;
    case 27:
        create_platform_node(2, 4, xx, yy);
        break;
    case 28:
        create_platform_node(2, 5, xx, yy);
        break;
    case 29:
        create_platform_node(2, 6, xx, yy);
        break;
        
    // platform 3
    case 32:
        create_platform_node(3, 1, xx, yy);
        var instance = instance_create(xx, yy, movingPlatform);
        with (instance)
        {
            platformId = 3;
        }
        break;
    case 33:
        create_platform_node(3, 2, xx, yy);
        break;
    case 34:
        create_platform_node(3, 3, xx, yy);
        break;
    case 35:
        create_platform_node(3, 4, xx, yy);
        break;
    case 36:
        create_platform_node(3, 5, xx, yy);
        break;
    case 37:
        create_platform_node(3, 6, xx, yy);
        break;
}

#define create_platform_node
/// create_platform_node(platform_id, node_id, x, y);

var platformId = argument0;
var nodeId = argument1 - 1;
var xx = argument2;
var yy = argument3;

var instance = instance_create(xx, yy, platformNode);

instance.platformId = platformId;
instance.nodeId = nodeId;